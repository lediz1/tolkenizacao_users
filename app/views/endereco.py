import pycep_correios
from validate_docbr import CPF
from werkzeug.security import generate_password_hash
from app import db
from flask import request, jsonify

from ..models.endereco import endereco_schema
from ..models.users import Users, user_schema, users_schema

def post_endereco():
    cep = request.json['cep']
    rua = request.json['logradouro']
    bairro = request.json['bairro']
    user_id = request.json['user_id']
    cidade = request.json['cidade']
    estado = request.json['estado']

    try:
        #endereco = endereco_schema.dump(pycep_correios.get_address_from_cep(request.json['cep']))
        endereco = pycep_correios.get_address_from_cep(request.json['cep'])
        print(endereco)
        print(endereco_schema.dump(endereco))
    except:
        return jsonify({'message': 'invalid CEP', 'data': {}})


    #print(username,password)

    user = user_by_username(username)

    if user:
        result = user_schema.dump(user)
        return jsonify({'message': 'user already exists', 'data': {}})

    pass_hash = generate_password_hash(password)
    #print(pass_hash)
    user = Users(username, pass_hash, name, email,cpf)
    #print(user.username,user.password,user.name,user.email,user.cpf)

    validar = CPF()
    if  validar.validate(cpf) is not True: # validar CPF
        return jsonify({'message': 'CPF invalid', 'data': {}}), 500



    try:
        db.session.add(user)
        db.session.commit()
        result = user_schema.dump(user)
        print(result['id'])
        return jsonify({'message': 'successfully registered', 'data': result}), 201
    except:
        return jsonify({'message': 'unable to create', 'data': {}}), 500
