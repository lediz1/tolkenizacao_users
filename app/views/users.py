import pycep_correios
from validate_docbr import CPF
from werkzeug.security import generate_password_hash
from app import db
from flask import request, jsonify

from ..models.endereco import endereco_schema, Endereco
from ..models.users import Users, user_schema, users_schema,users_schema2


"""Retorna lista de usuários"""


def get_users():
    #users = Users.query.all()
    users = Users.query.with_entities(Users.id,Users.username).all()

    if users:
        result = users_schema2.dump(users)
        return jsonify({'message': 'successfully fetched', 'data': result})

    return jsonify({'message': 'nothing found', 'data': {}})


"""Retorna usuário específico pelo ID no parametro da request"""

def get_user(id):
    user = Users.query.get(id)
    adress = endereco_schema.dump(Endereco.query.filter(Endereco.user_id == id).one())

    if user:
        result = user_schema.dump(user)
        result['endereco'] = adress
        return jsonify({'message': 'successfully fetched', 'data': result}), 201

    return jsonify({'message': "user don't exist", 'data': {}}), 404

"""Cadastro de usuários com validação de existência"""

def post_user():
    username = request.json['username']
    password = request.json['password']
    name = request.json['name']
    email = request.json['email']
    cpf = request.json['cpf']

    validar = CPF()
    if validar.validate(cpf) is not True:  # validar CPF
        return jsonify({'message': 'CPF invalid', 'data': {}}), 500

    try:
        endereco = pycep_correios.get_address_from_cep(request.json['cep'])
    except:
        return jsonify({'message': 'invalid CEP', 'data': {}})

    user = user_by_username(username)

    if user:
        return jsonify({'message': 'user already exists', 'data': {}})

    pass_hash = generate_password_hash(password)
    user = Users(username, pass_hash, name, email,cpf)

    try:
        db.session.add(user)
        db.session.commit()
        result = user_schema.dump(user)

        endereco['id'] = int(result['id'])
        endereco = Endereco(endereco['cep'],endereco['logradouro'],endereco['bairro'],endereco['id'],endereco['cidade'],endereco['uf'])
        db.session.add(endereco)
        db.session.commit()
        result['endereco'] = endereco_schema.dump(endereco)

        return jsonify({'message': 'successfully registered','Usuario': result,}), 201
    except:
        return jsonify({'message': 'unable to create', 'data': {}}), 500

"""Atualiza usuário baseado no ID."""


def update_user(id):
    username = request.json['username']
    password = request.json['password']
    name = request.json['name']
    email = request.json['email']
    user = Users.query.get(id)
    adress = Endereco.query.filter(Endereco.user_id == id).one()

    if not user:
        return jsonify({'message': "user don't exist", 'data': {}}), 404

    pass_hash = generate_password_hash(password)

    if user:
        try:
            endereco = pycep_correios.get_address_from_cep(request.json['cep'])
        except:
            return jsonify({'message': 'invalid CEP', 'data': {}})

        try:
            user.username = username
            user.password = pass_hash
            user.name = name
            user.email = email
            db.session.commit()

            adress.cep = endereco['cep']
            adress.bairro = endereco['bairro']
            adress.logradouro = endereco['logradouro']
            adress.cidade = endereco['cidade']
            adress.uf = endereco['uf']
            db.session.commit()


            result = user_schema.dump(user)
            result['endereco'] = endereco_schema.dump(endereco)
            return jsonify({'message': 'successfully updated', 'data': result}), 201
        except:
            return jsonify({'message': 'unable to update', 'data':{}}), 500


"""Deleta usuário com base no ID da request"""


def delete_user(id):
    try:
        user = Users.query.get(id)
        adress = Endereco.query.filter(Endereco.user_id == id).one()
    except:
        return jsonify({'message': "user don't exist", 'data': {}}), 404

    if user:
        try:
            db.session.delete(adress)
            db.session.commit()

            db.session.delete(user)
            db.session.commit()

            result = user_schema.dump(user)
            return jsonify({'message': 'successfully deleted', 'data': result}), 200
        except:
            return jsonify({'message': 'unable to delete', 'data': {}}), 500


def user_by_username(username):
    try:
        return Users.query.filter(Users.username == username).one()
    except:
        return None