import datetime
from functools import wraps

import jwt

import config
from app import app
from flask import request, jsonify
from .users import user_by_username

from werkzeug.security import check_password_hash


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')
        #print(token)
        if not token:
            return jsonify({'message': 'token is missing', 'data': []}), 401
        try:
            data = jwt.decode(token, config.keysecret, algorithms="HS256")
            #print(data)
            current_user = user_by_username(username=data['username'])
        except:
            return jsonify({'message': 'token is invalid or expired', 'data': []}), 401
        #print(*args,**kwargs)
        return f(current_user, *args, **kwargs)
    return decorated


# Gerando token com base na Secret key do app e definindo expiração com 'exp'
def auth():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return jsonify({'message': 'could not verify', 'WWW-Authenticate': 'Basic auth="Login required"'}), 401
    user = user_by_username(auth.username)

    if not user:
        return jsonify({'message': 'user not found', 'data': []}), 401

    if user and check_password_hash(user.password, auth.password):
        #print(user.username)
        #print(datetime.datetime.now() + datetime.timedelta(hours=12))
        #print(config.keysecret)
        token = jwt.encode({'username': user.username, 'exp': datetime.datetime.now() + datetime.timedelta(hours=12) },
                           config.keysecret, algorithm="HS256")

        #return jsonify({'message': 'Validated successfully', 'token':jwt.decode(token, config.keysecret, algorithms="HS256"),
         #               'exp': datetime.datetime.now() + datetime.timedelta(hours=12)})

        return jsonify({'message': 'Validated successfully', 'token': token,
                        'exp': datetime.datetime.now() + datetime.timedelta(hours=12)})

    return jsonify({'message': 'could not verify', 'WWW-Authenticate': 'Basic auth="Login required"'}), 401
