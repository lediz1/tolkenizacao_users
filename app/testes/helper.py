import datetime
import jwt
import pytest
from app import app as flask_app
import urllib3
import json

from config import keysecret


@pytest.fixture
def app():
    yield flask_app

@pytest.fixture
def client(app):
    return app.test_client()



def test_admin(app, client):
    url = "http://127.0.0.1:5000/v1/users"
    payload = {
        "username": "admin",
        "password": "12345678",
        "name": "admin",
        "email": "admin@gmail.com",
        "cpf": "934.759.780-59",
        "cep": "27310-010"
    }
    headers = urllib3.make_headers(basic_auth='admin:12345678')
    response = client.post(url, json=payload, headers=headers)
    assert response.status_code == 201


'''teste de acesso'''
def test_auth(app,client):
    url = "http://127.0.0.1:5000/v1/auth"
    payload = {}
    headers = urllib3.make_headers(basic_auth='admin:12345678')
    response = client.post(url, data=json.dumps(payload), headers=headers)
    assert response.status_code ==200

'''teste de geração de token '''
def test_auth_token(app,client):
        #gera a criptografia
        token = jwt.encode({'username': 'leo', 'exp': datetime.datetime.now() + datetime.timedelta(hours=12)},
                           keysecret, algorithm="HS256")
        #decripta o token
        tokend = jwt.decode(token, keysecret, algorithms="HS256")  # decodificação

        with open('tokenfile.txt', 'w') as out_file:
            out_file.write(str(token)+"\n")
            out_file.write(str(tokend))




