import pytest

from app import app as flask_app
import urllib3


from app.models.users import Users



@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client(app):
    return app.test_client()

'''teste de cadastro do user admin'''


'''teste de cadastro'''

def test_cadastro(app, client):
    url = "http://127.0.0.1:5000/v1/users"
    payload = {
        "username": "leoadmin",
        "password": "12345678",
        "name": "leonardoadmin",
        "email": "ledizadmin20@gmail.com",
        "cpf": "519.910.910-36",
        "cep": "27310-010"
    }
    headers = urllib3.make_headers(basic_auth='admin:12345678')
    response = client.post(url, json=payload, headers=headers)
    assert response.status_code == 201


def test_atualização(app, client):
    descending = Users.query.order_by(Users.id.desc())
    last_item = descending.first().id

    with open("tokenfile.txt","r") as f:
        firstline = str(f.readline().rstrip())

    url = "http://127.0.0.1:5000/v1/users/"+str(last_item)+"?token="+str(firstline)
    print(url)
    payload = {
        "username": "leo2",
        "password": "12345678",
        "name": "leonardo2",
        "email": "lediz2@gmail.com",
        "cpf": "506.482.550-18",
        "cep": "27310-010"
    }
    headers = urllib3.make_headers(basic_auth='leoadmin:12345678')
    response = client.put(url, json=payload, headers=headers)
    assert response.status_code == 201


def test_delecao(app, client):
    descending = Users.query.order_by(Users.id.desc())
    last_item = descending.first().id
    with open("tokenfile.txt", "r") as f:
        firstline = str(f.readline().rstrip())

    url = "http://127.0.0.1:5000/v1/users/"+str(last_item)+"?token="+str(firstline)
    print(url)
    payload = {
    }
    headers = ""
    response = client.delete(url, json=payload, headers=headers)
    assert response.status_code == 200