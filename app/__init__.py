import pymysql
from flask import Flask, jsonify
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
pymysql.install_as_MySQLdb()



app = Flask(__name__)
app.config.from_object('config')
app.config['JSON_SORT_KEYS'] = False
db = SQLAlchemy(app)
ma = Marshmallow(app)



from .models import users
from .models import endereco
from .routes import routes

from app import db
db.create_all()


