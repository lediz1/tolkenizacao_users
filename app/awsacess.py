import sys
import boto3
import os

ENDPOINT = "apiflask.cxaadwnwrzsj.us-west-1.rds.amazonaws.com"
PORT = "3306"
USR = "admin"
REGION = "us-east-1a"
os.environ['LIBMYSQL_ENABLE_CLEARTEXT_PLUGIN'] = '1'

# gets the credentials from .aws/credentials
session = boto3.Session(profile_name='Default')
client = session.client('rds')

token = client.generate_db_auth_token(DBHostname=ENDPOINT, Port=PORT, DBUsername=USR, Region=REGION)
print(token)
