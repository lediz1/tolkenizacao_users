import datetime

from app import db, ma



class Endereco(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    cep = db.Column(db.String(40), nullable=False)
    logradouro = db.Column(db.String(40), nullable=False)
    bairro= db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    #created_on = db.Column(db.DateTime, default=datetime.datetime.now())
    cidade = db.Column(db.String(40), nullable=False)
    uf = db.Column(db.String(40), nullable=False)
    user = db.relationship('Users', back_populates='endereco')

    def __init__(self, cep, logradouro, bairro,user_id,cidade,uf):
        self.cep = cep
        self.logradouro = logradouro
        self.bairro = bairro
        self.user_id = user_id
        self.cidade = cidade
        self.uf = uf


class EnderecoSchema(ma.Schema):

    class Meta:
        fields = ('id','bairro', 'cep','cidade','logradouro','uf','user_id')


endereco_schema = EnderecoSchema()
enderecos_schema = EnderecoSchema(many=True)
