# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

EXPOSE 5000
WORKDIR /home/leo/Documentos/app
COPY app app

COPY run.py run.py
COPY run.ini run.ini
COPY config.py config.py
COPY config.ini config.ini
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

CMD python run.py





