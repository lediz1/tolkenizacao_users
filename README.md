# Serviços
## _Tecnologias_

Para esse desenvolvimento foram usados o python, pycharm, postman,fedora 34,docker,mysql. Tdos os cpf utilziados nestes testes foram gerados pela pagina 
https://www.4devs.com.br/gerador_de_cpf, apenas com o intuito de testes e não deve ser utilizado em ambiente de produção ou para outros fins que não sejam testes.
Os ceps foram utilizados fazendo uma procura no google e assim como acima, usar apenas como testes e não em ambiente de produção.

Todas as collection usadas no postman se encontram na pasta "postman_collection". Prestar atenção principalmente nos argumentos em que o id do user no database devem ser passados(como no delete user,atualizar user e pesquisa 1 usuario) pois deve ser passado o id correto.       

## INSTALAÇÃO
# Postman
Para instalar o postman

-    $ sudo snap install postman

Todas as collection usadas no postman se encontram na pasta "postman_collection". Prestar atenção principalmente nos argumentos em que o id do user no database devem ser passados(como no delete user,atualizar user e pesquisa 1 usuario) pois deve ser passado o id correto.       


# Mysql
Instalar e Acessar o mysql:

-    $ sudo dnf -y install https://repo.mysql.com//mysql80-community-release-fc30-1.noarch.rpm

Para esse desenvolvimento, o banco de dados foi local.Acesse o mysql e crie o banco de dados
-    $ Create DATABASE apiflask;

Não precisa criar as tabelas(users e endereço).
Caso queira mudar o nome do database, acesse o "config.ini" e mude em "db" o nome do database e o password

Serão criadas duas tabelas ao iniciar o app:

- Users
- Endereço


Poderia ter sido criada só uma tabela com todos os dados, mas foi feito dessa forma para simular uso de chave estrangeira e comportamento e posteriores testes em separado para cadastro,alteração,etc.


# Docker
-    $ sudo dnf -y install dnf-plugins-core
-    $ sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
-    $ sudo dnf install docker-ce docker-ce-cli containerd.io
-    $ sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
-    $ sudo reboot
-    $ sudo systemctl start docker

     Executando o docker-compose.No terminal ir até a pasta onde se encontra o arquivo docker-compose.
-    $ docker-compose up

	

    
# Pycharm

Caso esteja usando o pycharm, basta abrir o projeto e executar o run.py.
Caso queira executar via terminal execute:  

- $ python run.py

Dentro do projeto ja existe uma venv criada que foi utlizada no projeto mas fica a criterio do usuario usar ou não uma outra venv de sua escolha.



## Utilização


# Cadastrar usuário 

Depois da aplicação rodando, o primeiro passo é pelo menos ter um usuário cadastrado na tabela de usuários.Todas as requisições foram feitas utilizando o POSTMAN

Endereço:
- http://127.0.0.1:5000/v1/users

Postman: na aba Body deve-se enviar um json
```json
{
	"username":"leo", 
	"password":"12345678", 
	"name":"leonardo10", 
	"email":"lediz10@gmail.com", 
	"cpf":"112.261.770-42", 
	"cep":"27310-010"
}
```

Validação:
- 1- Caso o cpf e/ou cep sejam invalidos, nada será cadastrado e haverá um retorno avisando.
- 2- Caso o json contenha algum campo que ja tenha valor pertencente a um usuario e seja unico, também ocorrerá um aviso e nada será
cadastrado.


A Validação acima também será verificada em caso de alteração dos dados.

# Gerando Token de autenticação
 Para Deletar e manipular as apis mais a frente, existe uma autenticação por token, para simular uma das varias formas de segurança dos dados.Para gerar o token ,segue abaixo o endereço e o procedimento no Postman.
 
Endereço:
- http://127.0.0.1:5000/v1/auth

Postman: na aba authorizathion(Basic auth), enviar o username e o password. Não é necessário enviar nenhum json. A saida será um json como esse:

```json
{
    "message": "Validated successfully",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImxlbyIsImV4cCI6MTYyMTQ1MzcwNX0.SBVb3VVf6d2DuU4HOjY1pP0X8R5BnP-HVTwuttgVp98",
    "exp": "Wed, 19 May 2021 19:48:25 GMT"
}
```

PS - Caso não tenha o postman, execute o helper.py em app/testes/helper.py.Copie o valor gerado e guarde.
O procedimento é meramente uma forma de demosntrar uma das varias formas de autenticação usando token,não sendo codificado nenhum método rigoroso 
ou real de segurança.O token gerado expira em 12 horas mas esse tempo pode ser alterado.


## Deletando,atualizando e listando

A partir de agora, com usuários cadastrados, podemos começar a manipular os dados ,utilizando
o token de permissão gerado anteriormente.

# Deletando usuário  
Endereço:
- http://127.0.0.1:5000/v1/users/1?token="token_gerado_anteriormente"

Postman - Para deletar,não é necessário enviar nenhum json mas é necessário enviar o token de autenticação como argumento e usar o método DELETE.


# Alterando informações do usuário
Endereço:
- http://127.0.0.1:5000/v1/v1/users/1?token="token_gerado_anteriormente"

Postman: usar metodo PUT e na aba Body deve-se enviar um json 
```json
{
	"username":"leo", 
	"password":"12345678",
	"name":"leonardo10", 
	"email":"lediz10@gmail.com", 
	"cpf":"112.261.770-42", 
	"cep":"27310-010"
}
```

# Visualizando os usuarios
Endereço: 
- http://127.0.0.1:5000/v1/users?token="token_gerado_anteriormente"

# Visualizando todas as informaçoes de um usuario
Endereço:
- http://127.0.0.1:5000/v1/users/1?token=="token_gerado_anteriormente"

Postman: usar metodo GET, não é necessário enviar nenhum json 



## Testes

Para os testes criados foram feitos alguns com criptografia usando o JWT,teste de acesso e alguns CRUD na tabela de users.

Para executar os testes  basta ir na pasta app/testes e executar os seguintes comandos:

-     $ pytest -s helper.py 
-     $ pytest -s user.py 

Os testes poderiam ter sido colocados em um arquivo somente mas dessa forma, eles se tornam mais faceis de serem identificados.


Para os testes de Crud, foram simulados testes de cadastro  atualização e deleção do mesmo.

O teste,helper.py, testa o cadastro do user admin, o acesso e depois a criptografia do sistema. Ao final, ele gera um arquivo chamado tokenfile.txt onde fica arquivado o usuario testado e o respectivo token gerado.

Ps- Lembrando que é necessario ter pelo menos o user admin cadastrado para se testar os "helper.py" . Lembre-se de apagar o usuario admin ao final dos testes  e o arquivo tokenfile.



Caso queira automatizar os testes, execute o arquivo executar.py que ele se encarregará de chamar todos os testes automaticamente ordenadamente

-     $ python user.py 








 

 








